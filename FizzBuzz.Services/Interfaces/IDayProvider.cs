﻿namespace FizzBuzz.Services.Interfaces
{
    using System;

    public interface IDayProvider
    {
        bool IsValid(DayOfWeek dayOfWeek);
    }
}
