﻿namespace FizzBuzz.Services.Interfaces
{
    public interface IFizzBuzzRule
    {
        bool IsValid(int number);

        string GetContent();
    }
}
