﻿namespace FizzBuzz.Services.Implementations
{
    using System;
    using FizzBuzz.Services.Constants;
    using FizzBuzz.Services.Interfaces;

    public class BuzzRule : IFizzBuzzRule
    {
        private readonly IDayProvider dayProvider;

        public BuzzRule(IDayProvider dayProvider)
        {
            this.dayProvider = dayProvider;
        }

        public bool IsValid(int number)
        {
            return number % Constants.BuzzDivisor == 0;
        }

        public string GetContent()
        {
            return this.dayProvider.IsValid(DateTime.Now.DayOfWeek) ? Constants.Wuzz : Constants.Buzz;
        }
    }
}
