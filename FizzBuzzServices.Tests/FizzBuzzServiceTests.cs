﻿namespace BusinessLogic
{
    using FizzBuzz.Services.Implementations;
    using FizzBuzz.Services.Interfaces;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;

    public class FizzBuzzServiceTests
    {
        private Mock<IFizzBuzzRule> fizzBuzzRule;

        private Mock<IFizzBuzzRule> fizzRule;

        private Mock<IFizzBuzzRule> buzzRule;

        private static object[] fizzBuzzDataSource =
        {
            new object[] { 3, new List<string> {"1", "2", "fizz"}},
            new object[] { 5, new List<string> {"1", "2", "fizz", "4", "buzz" }},
            new object[] { 15, new List<string> {"1", "2", "fizz", "4", "buzz", "fizz", "7", "8", "fizz", "buzz", "11", "fizz", "13", "14", "fizz buzz"}},
        };

        private static object[] BuzzOrWuzzDataSource =
     {
            new object[] { 3, new List<string> {"1", "2", "wizz"}},
            new object[] { 5, new List<string> {"1", "2", "wizz", "4", "wuzz" }},
            new object[] { 15, new List<string> {"1", "2", "wizz", "4", "wuzz", "wizz", "7", "8", "wizz", "wuzz", "11", "wizz", "13", "14", "wizz wuzz" } },
        };


        [TestCaseSource("fizzBuzzDataSource")]
        public void Service_Should_Return_FizzBuzzResult_BasedOnValuesProvided(int value, List<string> expected)
        {
            this.fizzBuzzRule = new Mock<IFizzBuzzRule>();
            this.fizzRule = new Mock<IFizzBuzzRule>();
            this.buzzRule = new Mock<IFizzBuzzRule>();
            this.fizzRule.Setup(x => x.IsValid(3)).Returns(true);
            this.fizzRule.Setup(x => x.IsValid(6)).Returns(true);
            this.fizzRule.Setup(x => x.IsValid(9)).Returns(true);
            this.fizzRule.Setup(x => x.IsValid(12)).Returns(true);
            this.fizzRule.Setup(x => x.GetContent()).Returns("fizz");

            this.buzzRule.Setup(y => y.IsValid(5)).Returns(true);
            this.buzzRule.Setup(y => y.IsValid(10)).Returns(true);
            this.buzzRule.Setup(x => x.GetContent()).Returns("buzz");

            this.fizzBuzzRule.Setup(z => z.IsValid(15)).Returns(true);
            this.fizzBuzzRule.Setup(z => z.GetContent()).Returns("fizz buzz");
            var service = new FizzBuzzService(new List<IFizzBuzzRule>() { this.fizzRule.Object, this.buzzRule.Object, this.fizzBuzzRule.Object });

            var result = service.GetResult(value);

            Assert.AreEqual(result, expected);
        }

        [TestCaseSource("BuzzOrWuzzDataSource")]
        public void Service_Should_Return_WizzWuzzResult_BasedOnValuesProvided(int value, List<string> expected)
        {
            this.fizzBuzzRule = new Mock<IFizzBuzzRule>();
            this.fizzRule = new Mock<IFizzBuzzRule>();
            this.buzzRule = new Mock<IFizzBuzzRule>();
            this.fizzRule.Setup(x => x.IsValid(3)).Returns(true);
            this.fizzRule.Setup(x => x.IsValid(6)).Returns(true);
            this.fizzRule.Setup(x => x.IsValid(9)).Returns(true);
            this.fizzRule.Setup(x => x.IsValid(12)).Returns(true);
            this.fizzRule.Setup(x => x.GetContent()).Returns("wizz");

            this.buzzRule.Setup(y => y.IsValid(5)).Returns(true);
            this.buzzRule.Setup(y => y.IsValid(10)).Returns(true);
            this.buzzRule.Setup(x => x.GetContent()).Returns("wuzz");

            this.fizzBuzzRule.Setup(z => z.IsValid(15)).Returns(true);
            this.fizzBuzzRule.Setup(z => z.GetContent()).Returns("wizz wuzz");
            var service = new FizzBuzzService(new List<IFizzBuzzRule>() { this.fizzRule.Object, this.buzzRule.Object, this.fizzBuzzRule.Object });

            var result = service.GetResult(value);

            Assert.AreEqual(result, expected);
        }
    }
}
